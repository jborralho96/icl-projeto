package values;

public interface MemoryManagement {
	RefValue newRef(IValue value);
	IValue get(RefValue ref);
	IValue set(RefValue ref, IValue value);
	//void free;

}
