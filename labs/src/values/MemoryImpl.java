package values;

public class MemoryImpl implements MemoryManagement {
	
	public static final MemoryImpl singleton = new MemoryImpl();
	
	public RefValue newRef(IValue value) {
		return new MemoryCell(value);
	}
	
	public IValue get(RefValue ref) {
		return ((MemoryCell) ref).value;
	}
	
	public IValue set(RefValue ref, IValue value) {
		return ((MemoryCell) ref).value = value;
	}
	
	//void free(RefValue ref) {}

}
