package values;

import java.util.List;

import ast.ASTId;
import ast.ASTNode;
import ast.Param;
import util.IEnvironment;

public class Closure implements IValue{
	ASTNode body;
	List<Param> params;
	IEnvironment<IValue> env;
	
	public Closure(ASTNode body,List<Param> params, IEnvironment<IValue> env) {
		this.body = body;
		this.params = params;
		this.env = env;
	}
	
	public List<Param> getParams() {
		return params;
	}
	
	public ASTNode getBody() {
		return body;
	}
	
	public IEnvironment<IValue> getEnv() {
		return env;
	}
	
	public void SetEnv(IEnvironment<IValue> newenv) {
		env = newenv;
	}
}
