package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import compiler.CodeBlock.StackFrame;
import types.BoolType;
import types.FunType;
import types.IType;
import types.IntType;
import types.RefType;

public class CodeBlock {


	//public final static  String dir = "/home/carrie/icl-projeto/labs/src/";
	public final static String dir = "/home/jambe/Documents/7ºSemestre/ICL/project/icl-projeto/labs/src/";

	public static class FunInterface {

		public int id;
		public String name;
		public IType type;
		public String apply;
		public FunInterface(int id, IType type,String apply) {
			this.id = id;
			this.name = "funtype_"+id;
			this.type=type;
			this.apply=apply;
		}

		public void dump() throws FileNotFoundException {

			PrintStream out = new PrintStream(new FileOutputStream(dir+name+".j"));
			out.println(".interface "+name);
			out.println(".super java/lang/Object");
			//inserir a variavel da ref
			out.println(".method abstract public " + apply);
			out.println(".end method");
		}
	}

	public static class ClosureClass{
		int id;
		StackFrame SL;
		String name;
		FunInterface signature;
		ArrayList<String> codigo;
		int nArgs;

		public ClosureClass(int id, StackFrame SL,FunInterface signature) {
			this.id=id;
			this.SL=SL;
			name = "closure_"+id;
			this.signature=signature;
			codigo = new ArrayList<String>(100);
			this.nArgs = 0;
		}

		public void dump() throws FileNotFoundException {
			PrintStream out = new PrintStream(new FileOutputStream(dir+name+".j"));
			out.println(".class public "+name);
			out.println(".super java/lang/Object");
			out.println(".implements "+signature.name);
			//verificar se tem SL
			if(SL!=null) 
				out.println(".field public SL L"+SL.frameName+";");

			String apply = signature.apply;
			out.println(".method public "+apply);
			out.println(".limit locals 10");
			out.println(".limit stack 256");
			//fazer dump do codigo privado
			dumpCodigo(out);

			//verificar se o tipo de retorno da funcao e booleano ou objecto
			String ReturnString = apply.substring(apply.length() - 1);
			if(ReturnString.equals("I") || ReturnString.equals("Z"))
				out.println("ireturn");
			else out.println("return");

			out.println(".end method");

			out.println(".method public <init>()V");
			out.println("	aload_0");
			out.println("	invokenonvirtual java/lang/Object/<init>()V");
			out.println("	return");
			out.println(".end method");

		}
		void dumpCodigo(PrintStream out) {
			for( String s : codigo )
				out.println("       "+s);
		}
	}
	public static class StackFrame {

		String id; //identificador do stack frame
		StackFrame previous; //identificador do stack frame anterior
		String frameName;
		ArrayList<String> locs;
		int nArgs;
		ClosureClass closure;

		StackFrame(String id,ArrayList<String>ids,StackFrame previous,ClosureClass closure) {
			this.id = id;
			this.previous=previous;
			this.frameName ="frame_"+id;
			locs = new ArrayList<String>();
			this.closure = closure;

			//			for (int i = 0;i<ids.size();i++){
			//				locs.add("loc_"+i+ " "+ids.get(i));
			//			}	
			nArgs = 0;
			//se nao tiver dentro de uma closure o numero de argumentos = 0

		}


		public void setArgumentsNumber(int n) {
			this.nArgs = n;
		}

		void dump() throws FileNotFoundException {

			PrintStream out = new PrintStream(new FileOutputStream(dir+frameName+".j"));
			out.println(".class "+frameName);
			out.println(".super java/lang/Object");

			//verificar se existe uma frame anterior
			if(this.previous != null)
				out.println(".field public SL L"+"frame_"+previous.id+";");

			for (String s:locs) 
				out.println(".field public "+s);


			out.println(".method public <init>()V");
			out.println("   aload_0");
			out.println("   invokenonvirtual java/lang/Object/<init>()V");
			out.println("   return");
			out.println(".end method");
		}
	}


	static class RefClass {
		//		int id; //sequencial

		//ref_int
		String name = "";
		IType field;
		IType type;

		public RefClass (IType t) {
			//se for var(var()), temos ref_int_int por exemplo

			name = getName(t);
			type = t;
			field =((RefType) t).getType();
		}

		String getName(IType t) {
			
			String name="";

			if( !(t instanceof RefType)) {
				if(t == IntType.singleton)
					name+="I";
				else if(t == BoolType.singleton)
					name+="Z";
			} 

			else {

				name ="ref";
				

				while(t instanceof RefType) {

					IType tipo = ((RefType) t).getType();

					if(tipo == IntType.singleton)
						name+="_int";
					else if(tipo == BoolType.singleton)
						name+="_bool";
					else 
						name+="_ref";
					t = tipo;
				}
			}
			return name;
		}

		void dump() throws FileNotFoundException {

			PrintStream out = new PrintStream(new FileOutputStream(dir+name+".j"));
			out.println(".class "+name);
			out.println(".super java/lang/Object");

			//inserir a variavel da ref
			String fiel = getName(this.field);
			if (fiel.length()<=1)
				out.println(".field public value " + fiel);
			else out.println(".field public value L" + fiel+";");

			out.println(".method public <init>()V");
			out.println("	aload_0");
			out.println("	invokespecial java/lang/Object/<init>()V");
			out.println("	return");
			out.println(".end method");

		}
	}

	ArrayList<String> code;
	ArrayList<String> previousCode;
	Stack<StackFrame> frames;
	ArrayList<StackFrame> allFrames;
	HashMap<IType,RefClass> refs; //refclass vai implementar o ref00, ref01, ref02, etc...
	//redefenir o hashcode do Itype (toString().hashCode) 
	HashMap<IType,FunInterface> funs;
	ArrayList<ClosureClass> closures;

	//funs.put(new Fun(int,int), new FunInterface(0))
	//funs.put(new Fun(Fun(int,int), new FunInterface(1))

	StackFrame current;
	StackFrame previous;

	public static final LabelFactory labelFactory = new LabelFactory();

	public CodeBlock() {
		code = new ArrayList<String>(100);
		previousCode = new ArrayList<String>(100);
		frames = new Stack<StackFrame>();
		allFrames = new ArrayList<StackFrame>(100);
		refs = new HashMap<IType,RefClass>();
		funs = new HashMap<IType,FunInterface>();
		closures = new ArrayList<ClosureClass>();
	}

	public void emit_push(int n) {
		code.add("sipush "+n);
	}

	public void emit_add() {
		code.add("iadd");
	}

	public void emit_mul() {
		code.add("imul");
	}

	public void emit_div() {
		code.add("idiv");
	}

	public void emit_sub() {
		code.add("isub");
	}

	public void emit_greaterThen() {
		code.add("if_icmpg");
	}

	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		out.println("       ; END");
		out.println("");
		out.println("");
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");
		out.println("       return");
		out.println("");
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for( String s : code )
			out.println("       "+s);
	}

	public void dump(String filename) throws FileNotFoundException {
		PrintStream out = new PrintStream(new FileOutputStream(filename));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
		dumpFrames();
		dumpRefs();
		dumpInterfaces();
		dumpClosures();

	}



	private void dumpFrames() throws FileNotFoundException {
		for( StackFrame frame: allFrames)
			frame.dump();
	}

	private void dumpRefs() throws FileNotFoundException{
		for (RefClass value: refs.values())
			value.dump();
	}

	private void dumpInterfaces() throws FileNotFoundException {
		for(FunInterface value:funs.values())
			value.dump();
	}

	private void dumpClosures() throws FileNotFoundException {
		// TODO Auto-generated method stub
		for(ClosureClass closure :closures)
			closure.dump();

	}


	public StackFrame newFrame(ArrayList<String> nodes,ClosureClass closure) {
		//verificar se a lista de stack frames esta vazia
		//criar uma nova stack frame

		StackFrame frame;

		//verificar se o current é nulo
		if(current!=null) {
			String id  =""+ (allFrames.size()+1);
			StackFrame previous = current;
			frame = new StackFrame(id,nodes,previous,closure);
			allFrames.add(frame);

			code.add("new frame_"+frame.id);
			code.add("dup");
			code.add("invokespecial frame_"+frame.id+"/<init>()V");
			//			/*dup
			//			aload SP
			//			putfield frame_id/SL Lframe_up;*/
			//
			//			code.add("dup");
			//			int SP = frame.nArgs+1;
			//			code.add("aload "+SP);
			//			code.add("putfield frame_"+frame.id+"/SL Lframe_"+frame.previous.id+"; ;pop(value and object ref)");

		}
		else {
			String id  =""+ (allFrames.size()+1);
			frame = new StackFrame(id,nodes,null,closure);
			allFrames.add(frame);
			code.add("new frame_"+frame.id);
			code.add("dup");
			code.add("invokespecial frame_"+frame.id+"/<init>()V");
			//			code.add("dup");
		}
		frames.push(frame);
		return frame;
	}

	public void find(int jumps, int loc, IType type) {
		//jumps to find the right frame

		int i = 0;
		StackFrame f = current;

		while(i<jumps) {
			if(f.previous!=null){
				code.add("getfield frame_"+f.id+"/SL Lframe_"+(f.previous.id)+"; ;pop(objectRef), push(value)");
			}
			i++;
			f = f.previous;
		}
		String typeString = jasmin_type(type);

		code.add("getfield frame_"+f.id+"/loc_"+loc+ " "+ typeString +" ;pop(objectRef), push(value)");

	}

	public   String jasmin_type(IType type) {
		String typeString= "";

		if(type instanceof RefType){
			typeString +="Lref";
			while(type instanceof RefType){

				IType t = ((RefType) type).getType();
				if(t == IntType.singleton)
					typeString+= "_int";
				else if(t == BoolType.singleton)
					typeString+= "_bool";
				else typeString+="_ref"; 
				type = t;
			}
			typeString +=";";
		}
		else if(type instanceof FunType) {
			typeString +="L";
			String funT = funs.get(type).name;
			typeString +=funT+";";
		}
		else {

			if(type == IntType.singleton)
				typeString= "I";
			if(type == BoolType.singleton)
				typeString= "Z";
		}
		return typeString;
	}

	public void emit_bool(boolean val) {
		if(val)
			code.add("iconst_1");
		else
			code.add("iconst_0");
	}

	public void emit_greaterThen(String label) {
		code.add("if_icmpgt " + label);
	}

	public void emit_greaterEqualThen(String label) {
		code.add("if_icmpge " + label);
	}

	public void emit_ifeq(String label) {
		code.add("ifeq "+label);
	}

	public void emit_ifneq(String label) {
		// TODO Auto-generated method stub
		code.add("ifne "+label);
	}

	public void emit_jump(String label) {
		code.add("goto "+label);
	}

	public void emit_if_boolean_goto(boolean value,String label) {
		code.add("if "+value + " goto "+label);
	}

	public void emit_if_boolean_jump(boolean value,String label){
		code.add("if "+value +" jmp "+label);
	}

	public void emit_anchor(String label) {
		code.add(label + ":");
	}

	public void emit_ifequal(String label) {
		//place a  label
		code.add("if_icmpeq " +label);
	}

	public void emit_lessThen(String label) {
		code.add("if_icmplt " + label);
	}

	public void emit_lessEqualThen(String label) {
		code.add("if_icmple " + label);
	}

	public void emit_ifNotequal(String label) {
		//place a  label
		code.add("if_icmpne " +label);
	}

	public void emit_pop() {
		// TODO Auto-generated method stub
		code.add("pop");
	}

	public void emit_dup() {
		code.add("dup");
	}

	//metodo que faz dup
	//sipush da definicao
	//putfield frame/loc_ tipo
	//faz astore do SP
	public void declaration(int id, IType type) {
		StackFrame SP = frames.peek();
		String typeString = jasmin_type(type);
		code.add("putfield frame_"+SP.id+"/loc_"+id + " "+typeString +" ;pop(value + object_Ref)"); //verificar o tipo do def
	}

	public void storeSP() {

		StackFrame frame = frames.peek();
		int SP = frame.nArgs+1;
		code.add("astore "+SP); //colocar o astore do SP
	}

	//alterar o aload
	public void getCurrentFrame() {
		int SP = current.nArgs+1;
		code.add("aload "+SP);
		code.add("checkcast frame_"+current.id);
	}

	public void putFrameInList() {
		previous = current;
		current = frames.peek();
	}


	public void createRef(IType t) {
		RefClass ref = new RefClass(t);

		//verificar se a string já existe
		if(!refs.containsKey(ref.type)) {
			refs.put(ref.type, ref);
		}
		//se ainda nao existir, junta-se a lista de referencias (refs)
	}

	// this corresponds to the endScope
	/*
			aload SP
			checkcast frame_id
			getfield frame_id/SL Lframe_up;
			astore SP
	 */
	public void finalize() {

		StackFrame f = frames.pop();
		int SP = f.nArgs+1;
		if(f.previous!=null){

			code.add("aload "+SP);
			code.add("checkcast frame_"+f.id);
			code.add("getfield frame_"+f.id+"/SL Lframe_"+f.previous.id+"; ;pop(objectRef) , push(value)");
			code.add("astore "+SP);
		}
		else {
			code.add("aconst_null");
			code.add("astore "+SP);
		}
		current = previous;
	}


	//new "refclass"
	//dup
	//invokspecial init
	public void initReference(IType type) {
		// TODO Auto-generated method stub
		String refName = refs.get(type).name;
		code.add("new "+refName);
		code.add("dup");
		code.add("invokespecial " + refName+"/<init>()V");
		code.add("dup");
	}

	public void putField(IType type) {
		// TODO Auto-generated method stub
		RefClass ref = refs.get(type);
		String refName = ref.name;
		String field = jasmin_type(ref.field);
		code.add("putfield "+refName + "/value " + field +" ;pop(value + objectRef)");
	}

	//metodo que faz 	
	//getfield refclass/value tj 
	public void getReference(IType type) {
		RefClass ref = refs.get(type);
		String name = ref.name;
		String field = jasmin_type(ref.field);
		code.add("getfield "+name+"/value "+field +" ; pop(objectRef), push(value)");
	}


	public void emitInvokeInterface(String string, IType funType) {

		//devolver a interface que implementa este tipo de funcoes

		String funcao = funs.get(funType).name;

		List<IType> types = ((FunType) funType).paramTypesT();
		IType result = ((FunType) funType).getResultT();

		// TODO Auto-generated method stub
		String arguments ="apply(";
		List<String> jasm = new LinkedList<String>();
		int argsize =0;
		for(IType t :types){
			arguments+=jasmin_type(t);
			argsize++;
		}
		arguments +=")" + jasmin_type(result);
		code.add("invokeinterface " +funcao +"/"+arguments +" "+ ++argsize);

		System.out.println(arguments);

	}

	public  String apply(IType t) {
		String arguments ="apply(";
		FunType f = (FunType) t;
		List<String> jasm = new LinkedList<String>();
		LinkedList<IType> jasmines = f.paramTypesT();
		for(IType tipo :jasmines)
			arguments+=(jasmin_type(tipo));

		arguments +=")" + jasmin_type(f.getResultT());

		System.out.println(arguments);
		return arguments;

	}

	//searches "funs" and gets the interface, or creates the
	//.interface funtype_X file with the signature
	public FunInterface createOrReuse(FunType type) {
		if(funs.containsKey(type))
			return funs.get(type);
		else {
			int size = funs.size();
			String apply = apply(type);
			FunInterface fun = new FunInterface(size,type,apply);
			funs.put(type, fun);
			return fun;
		}

	}

	//creates the class, computes the SP
	//to the defining context
	public ClosureClass createClosure(FunInterface closureSignature) {
		//contador de closures
		int id = closures.size();
		//devolver a frame mais recente
		StackFrame SP =current;
		ClosureClass closure = new ClosureClass(id,SP,closureSignature);
		closures.add(closure);
		return closure;
	}

	// new closure_0 -- dup -- init -- dup -- aload SP -- putfield SL
	// to the closure context
	public void emitnewClosure(ClosureClass closureCode) {
		String closureName = closureCode.name;
		code.add("new "+closureName);
		emit_dup();
		code.add("invokespecial "+ closureName +"/<init>()V" );

		if(closureCode.SL!=null){
			emit_dup();
			code.add("aload "+(closureCode.nArgs+1));
			code.add("putfield "+closureName +"/SL L"+closureCode.SL.frameName+"; ;pop(value+objectRef)");
		}
	}
	//emit inside closure 
	//initialize arguments and SL inside closure
	public void pushClosure(ClosureClass closureCode,List<IType> tipoParametros) {
		// TODO Auto-generated method stub
		//cada closure vai ter algo semelhante ao array  de strings code
		//criar uma frame nove
		this.previousCode = this.code;
		this.code = closureCode.codigo;
		ArrayList<String> tipos = new ArrayList<String>();
		ArrayList<IType> ty = new ArrayList<IType>();
		for(IType t:tipoParametros){
			tipos.add(jasmin_type(t));
			ty.add(t);
		}

		StackFrame frame =newFrame(tipos,closureCode);
		this.setArgumentsFrame(frame, ty);

		//verificar se a frame tem SL (mais propriamente a closure
		if(closureCode.SL!=null){
			code.add("dup");
			code.add("aload 0");
			code.add("getfield "+closureCode.name+"/SL L"+closureCode.SL.frameName+";");
			code.add("putfield "+frame.frameName+"/SL L"+closureCode.SL.frameName+";" );
		}

		//buscar os argumentos

		int i = 1;
		for (String t:frame.locs){
			code.add("dup");
			code.add("iload "+i);
			code.add("putfield "+frame.frameName+"/" +t +" ;pop(value + objectRef)");
			i++;
		}
		//guardar o SP (numero de argumentos + 1)
		int nArgumentos = frame.locs.size();
		frame.setArgumentsNumber(nArgumentos);

		//	code.add("astore "+(nArgumentos+1));

	}

	public void popClosure() {
		// TODO Auto-generated method stub
		this.finalize();
		this.code = this.previousCode;
	}


	public void setArgumentsFrame(StackFrame frame,ArrayList<IType>ids) {
		for (int i = 0;i<ids.size();i++){
			String jas = jasmin_type(ids.get(i));
			frame.locs.add("loc_"+i+ " "+jas);
		}	

	}

	public void initializeSL(StackFrame frame) {
		if(current!=null){
			// TODO Auto-generated method stub
			code.add("dup");
			int SP = frame.nArgs+1;
			code.add("aload "+SP);
			code.add("putfield frame_"+frame.id+"/SL Lframe_"+frame.previous.id+"; ;pop(value and object ref)");
		}

	}



}
