package types;

import java.util.LinkedList;

public class FunType implements IType {
	
	LinkedList<IType> types;
	IType result;
	//o funtype tem que ter um resultado 
	// e argumentos
	
	public FunType(LinkedList<IType> t) {
		this.types=t;
		this.result = types.removeLast();
	}
	
	public LinkedList<IType> paramTypesT() {
		return types;
	}
		
	public IType getResultT(){
		return result;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof FunType)) 
			return false;
		
		FunType fun= (FunType) o;
		int oSize = fun.types.size();
		//verificar se o tipo dos argumentos sao iguais aos do 
		if(oSize!=this.types.size())
			return false;
		
		for(int i=0;i<oSize;i++){
			if(fun.types.get(i)!=this.types.get(i))
				return false;
		}
		if(!fun.result.equals(this.result))
			return false;
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + types.hashCode();
		return result;
	}
}
