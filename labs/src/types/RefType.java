package types;

public class RefType implements IType {
	
	IType type;
	
	public RefType(IType t) {
		this.type=t;
	}
	
	public IType getType() {
		return type;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof RefType)) 
			return false;
		
		RefType ref = (RefType) o;
		System.out.println("equals");
		System.out.println("o "+ ref.type);
		System.out.println("this " + type);
		return ref.type.equals(type);
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + type.hashCode();
		return result;
	}
	

}
