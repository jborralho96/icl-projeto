.class public Demo
.super java/lang/Object

;
; standard initializer
.method public <init>()V
   aload_0
   invokenonvirtual java/lang/Object/<init>()V
   return
.end method

.method public static main([Ljava/lang/String;)V
       ; set limits used by this method
       .limit locals 10
       .limit stack 256

       ; setup local variables:

       ;    1 - the PrintStream object held in java.lang.out
       getstatic java/lang/System/out Ljava/io/PrintStream;

       ; place your bytecodes here
       ; START

       new frame_1
       dup
       invokespecial frame_1/<init>()V
       dup
       sipush 1
       putfield frame_1/loc_0 I ;pop(value + object_Ref)
       astore 1
       new frame_2
       dup
       invokespecial frame_2/<init>()V
       dup
       aload 1
       putfield frame_2/SL Lframe_1; ;pop(value and object ref)
       dup
       new closure_0
       dup
       invokespecial closure_0/<init>()V
       dup
       aload 1
       putfield closure_0/SL Lframe_1; ;pop(value+objectRef)
       putfield frame_2/loc_0 Lfuntype_0; ;pop(value + object_Ref)
       astore 1
       new frame_4
       dup
       invokespecial frame_4/<init>()V
       dup
       aload 1
       putfield frame_4/SL Lframe_2; ;pop(value and object ref)
       dup
       new closure_1
       dup
       invokespecial closure_1/<init>()V
       dup
       aload 1
       putfield closure_1/SL Lframe_2; ;pop(value+objectRef)
       putfield frame_4/loc_0 Lfuntype_1; ;pop(value + object_Ref)
       astore 1
       aload 1
       checkcast frame_4
       getfield frame_4/loc_0 Lfuntype_1; ;pop(objectRef), push(value)
       aload 1
       checkcast frame_4
       getfield frame_4/SL Lframe_2; ;pop(objectRef), push(value)
       getfield frame_2/loc_0 Lfuntype_0; ;pop(objectRef), push(value)
       invokeinterface funtype_1/apply(Lfuntype_0;)I 2
       aload 1
       checkcast frame_4
       getfield frame_4/SL Lframe_2; ;pop(objectRef) , push(value)
       astore 1
       aload 1
       checkcast frame_2
       getfield frame_2/SL Lframe_1; ;pop(objectRef) , push(value)
       astore 1
       aconst_null
       astore 1
       ; END


       ; convert to String;
       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
       ; call println 
       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

       return

.end method
