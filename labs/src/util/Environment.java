package util;
import java.util.*;

public class Environment<T> implements IEnvironment<T> {

	static class Assoc<T> {//(binding)
		String id;
		T value;
		
		Assoc(String id, T value) {
			this.id = id;
			this.value = value;
		}
	}

	Environment<T> up;//
	ArrayList<Assoc<T>> assocs;//cada objeto da classe environment vai impleentar u nivel de environemnt
	
	public Environment() {//um environment e um nivel da stack
		this.up = null;
		this.assocs = new ArrayList<Assoc<T>>();//associacoes locais
	}
	
	private Environment(Environment<T> up) {//uma insercao a cabeca de uma lista ligada, usado no begin scope
		this();
		this.up = up;
	}

	public T find(String id) throws UndeclaredIdentifierException {
		Environment<T> current = this;//apontador para o nivel, vai andando para cima
		while(current != null) {
			for(Assoc<T> assoc: current.assocs)
				if( assoc.id.equals(id))
					return assoc.value;//location in here
			//njumps in here
			current = current.up;
		}
		throw new UndeclaredIdentifierException(id);
	}
	
	public Address lookup(String id) throws UndeclaredIdentifierException {
		Environment<T> current = this;//apontador para o nivel, vai andando para cima
		int jumps = 0;
		int offset = 0;
		while(current != null) {
			for(Assoc<T> assoc: current.assocs) {
				if( assoc.id.equals(id))
					return new Address(jumps, offset);
				offset++;
			}
			offset = 0;
			jumps++;
			current = current.up;
		}
		throw new UndeclaredIdentifierException(id);
	}
	
	public Environment<T> beginScope() {
		return new Environment<T>(this);
	}
	
	public Environment<T> endScope() {//devolve o no anterior
		return up;
	}
	
	public void assoc( String id, T value ) throws DuplicateIdentifierException {

		for(Assoc<T> assoc: assocs)
			if(assoc.id.equals(id))
				throw new DuplicateIdentifierException(id);
		
		assocs.add(new Assoc<T>(id,value));
		
	}
}


