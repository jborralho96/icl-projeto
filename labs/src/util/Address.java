package util;

public class Address {
	
	
	int jumps; //numero de saltos 
	int loc;
	
	public Address(int jumps, int loc) {
		this.jumps = jumps;
		this.loc = loc;
	}
	
	public int getJumps() {
		return jumps;
	}
	
	public int getLoc() {
		return loc;
	}

}
