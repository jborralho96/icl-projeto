package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import main.Console;
import parser.ParseException;
import types.BoolType;
import types.IType;
import types.IntType;
import types.RefType;
import values.BoolValue;
import values.IntValue;

public class TypeCheckingTests {

	private void testCase(String expression, IType type) throws ParseException {
		assertTrue(Console.acceptCompareTypes(expression,type));
	}

	private void testNegativeCase(String expression, IType type) throws ParseException {
		assertFalse(Console.acceptCompareTypes(expression, type));
	}

	private void testCaseBool(String expression, boolean value) throws ParseException {
		assertTrue(Console.acceptCompareBool(expression, new BoolValue(value)));
	}

	// private void testNegativeCaseBool(String expression, boolean value) throws ParseException {
	// 	assertFalse(Console.acceptCompareBool(expression, new BoolValue(value)));
	// }

	@Test
	public void test01() throws Exception {
		testCase("1;;",IntType.singleton);
		testCase("99;;",IntType.singleton);
		testCase("1+2;;",IntType.singleton);
		testCase("1-2-3;;",IntType.singleton);
	}

	@Test
	public void testsLabClass02() throws Exception {
		testCase("true;;",BoolType.singleton);
		testCase("false;;",BoolType.singleton);
		testCase("11 == 11;;",BoolType.singleton);
		testCase("11 < 22;;",BoolType.singleton);
		testCase("11 > 22;;",BoolType.singleton);
	}

	@Test
	public void testsLabClass05() throws Exception {
		
		testCase("var(0);;",new RefType(IntType.singleton));
		
//		testCase("var(0):=1;;",IntType.singleton);
//		testCase("decl x = 1 in x+1 end;;",IntType.singleton);
//		testCase("decl x = 1 in decl y = 2 in x+y end end;;",IntType.singleton);
//		testCase("decl x = decl y = 2 in 2*y end in x*3 end;;",IntType.singleton);
//		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end;;", IntType.singleton);
//		testNegativeCase("x+1", IntType.singleton);
//		testNegativeCase("decl x = 1 in x+1 end + x", IntType.singleton);
//		//testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y+x end;;", IntType.singleton);
//		testCase("1;var(0):=1;;", IntType.singleton);
//		testCase("decl x = 1 in x + 2 end ; var(0):=1;;",IntType.singleton);
//		
//		
//		//cenas
//		
//		
//		
//		testCase("*var(0);;",IntType.singleton);
//		testCase("*var(99);;",IntType.singleton);
//		testCase("var(0):=1;;",IntType.singleton);
//		testCase("decl x = var(0) in x := 1; *x end;;",IntType.singleton);
//		testCase("decl x = var(0) in decl y = x in x := 1; *y end end;;",IntType.singleton);
//		testCase("decl x = var(0) in decl y = var(0) in x := 1; *y end end;;",IntType.singleton);
//		testCase("decl x = true in if (x) then 5 else 6 end end;;", IntType.singleton);	
//		testCase("decl x = var(0) in decl y = var(0) in while *x < 10 do y := *y + 2; x := *x + 1 end end end;;",BoolType.singleton);
//		testCase("if true then 1 else 2 end;;",IntType.singleton);
//		testCase("if false then 1 else 2 end;;", IntType.singleton);
//		testCase("if true then true else 1 end;;", BoolType.singleton); // type test
//		testCase("decl x = var(3) in decl y = var(1) in while *x > 0 do y := *y * *x; x := *x - 1 end end end;;",BoolType.singleton);
		
	}
	
	public void testFunction() throws Exception {
        testCase("decl f = fun -> 1 end in f() + 1 end;;", IntType.singleton);
        testCase("decl f = fun x : int -> x + 1 end in f(1) end;;", IntType.singleton);    
        testCase("(fun f:FunT(int,int,int) -> f (2,3) end) (fun x:int, y:int -> x+y end);;", IntType.singleton);
        testCase("decl f = (fun f:FunT(int,int,int) -> f (2,3) end) in f(fun x:int, y:int -> x+y end)end;;", IntType.singleton);
        testCase("decl f = fun x : int -> x + 1 end in decl g = fun f : FunT(int,int) -> f(1) end in g (f) end end;;", IntType.singleton);
        testCase("decl f = fun x : int, y : int -> x + y end in f (1,2) + f (1,3) end;;", IntType.singleton);
        testCase("decl f = fun x : int -> fun y : int -> x + y end end in decl g = f (1) in g (2) + g(3) end end;;", IntType.singleton);
        testCase("fun f:FunT(int,int) -> f(2) end (fun x:int-> x + 1 end);;",IntType.singleton);
        testNegativeCase("fun x:int -> x+1 end (true)",IntType.singleton);;
    }
	
	@Test
    public void testFunctionEquality() throws Exception {
        testCase("decl x = 1 in x + 1 end == (fun x:int -> x+1 end)(1);;", BoolType.singleton);
        testCase("decl x = 1 in x + 1 end == decl x = (fun x:int -> x+1 end) in x(1) end;;", BoolType.singleton);
        testCase("(fun x:int -> x+1 end)(1) == decl x = (fun x:int -> x+1 end) in x(1) end;;", BoolType.singleton);
    }
}
