package tests;

import org.junit.Test;

import main.Console;
import parser.ParseException;
import types.IntType;
import values.BoolValue;
import values.IValue;
import values.IntValue;

import static org.junit.Assert.*;

public class InterpreterTests {
	private void testCase(String expression, IValue value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));
	}

	private void testNegativeCase(String expression, IValue value) throws ParseException {
		assertFalse(Console.acceptCompare(expression,value));
	}

	private void testCaseBool(String expression, boolean value) throws ParseException {

		assertTrue(Console.acceptCompareBoolInter(expression, new BoolValue(value)));
	}

	private void testCaseNegativeBool(String expression, boolean value) throws ParseException {
		assertFalse(Console.acceptCompareBoolInter(expression, new BoolValue(value)));
	}

	//	@Test
	//	public void test01() throws Exception {
	//		testCase("1\n",new IntValue(1));
	//		testCase("1+2\n",new IntValue(3));
	//		testCase("1-2-3\n",new IntValue(-4));
	//	}

	@Test
	public void testsLabClass02() throws Exception {
		testCase("4*2;;",new IntValue(8));
		testCase("4/2/2;;",new IntValue(1));
		testCase("-1;;",new IntValue(-1));
		testCase("4/2*3+1-1;;",new IntValue(6));
		// more tests for boolean values
		testCaseBool("true;;", true);
		testCaseBool("!true;;",false);

		testCaseBool("false;;",false);
		testCaseBool("11 < 22;;", true);
		testCaseBool("11 > 22;;", false);
		testCaseBool("11 == 22;;", false);
		testCaseBool("3*5 != 12 == true;;", true);
		testCaseBool("1 == 2 && 3 == 4;;", false);
		testCaseBool("1!=3 || 1==1;;",true);
		testCaseBool("true && false;;",false);
		//testCaseNegativeBool("1 == 2 || 3 == 4 && xpto \n", true);
		//testCaseNegativeBool("!(1 == 2) && xpto \n", true);
	}

	@Test
	public void testsLabClass05() throws Exception {
		testCase("decl x = 1 in x+1 end;;",new IntValue(2));
		testCase("decl x = 1 in decl y = 2 in x+y end end;;",new IntValue(3));
		testCase("decl x = decl y = 2 in 2*y end in x*3 end;;",new IntValue(12));
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end;;", new IntValue(6));
		testCase("decl x=1 y=2 in x+y end;;", new IntValue(3));
		testCase("decl x=1 in decl y=2 in decl z=x+y in z+y+x end end end;;", new IntValue(6));
		testCase("decl x = 6 in decl x = decl z = 2 in z * z * z end y = decl z = 7 + x in z * x end in y + x end end;;", new IntValue(86));
		testCase("decl y = decl x = 3 in x * 4 end in y + 2 end;;", new IntValue(14));
		testCase("decl x = 6 in decl x = 3 y = decl z = 7 + x in z * x end in y + x end end;;", new IntValue(81));
		testNegativeCase("x+1;;", new IntValue(0));
		testNegativeCase("decl x = 1 in x+1 end + x;;", new IntValue(0));
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end;;", new IntValue(6));
	}

	@Test
		public void testsLabClass07() throws Exception {

			testCase("*var(0);;",new IntValue(0));
			testCase("*var(99);;",new IntValue(99));
			testCase("var(0):=1;;",new IntValue(1));
//			testCase("decl x=var(0) y=var(true) z=var(x)w=var(var(1))in w:=x z:=*w end;;");
			testCase("decl x = var(0) in x := 1; *x end;;",new IntValue(1));
			testCase("decl x = var(0) in decl y = x in x := 1; *y end end;;",new IntValue(1));
			testCase("decl x = var(0) in decl y = var(0) in x := 1; *y end end;;",new IntValue(0));
			testCase("decl x = true in if (x) then 5 else 6 end end;;", new IntValue(5));
			testCaseBool("decl x = var(0) in decl y = var(0) in while *x < 10 do y := *y + 2; x := *x + 1 end end end;;",false);
			testCase("if true then 1 else 2 end;;", new IntValue(1));
			testCase("if false then 1 else 2 end;;", new IntValue(2));
			testNegativeCase("if true then true else 1 end;;", new IntValue(0)); // type test
			testCaseBool("decl x = var(3) in decl y = var(1) in while *x > 0 do y := *y * *x; x := *x - 1 end end end;;",false);
			testCase("fun x:int -> x end(1);;",new IntValue(1));
			testCase("fun y:FunT(int,int) -> y(1) end (fun x:int -> x end);;",new IntValue(1));
			testCase("fun f:FunT(int,int), x:int -> f(x,1) end((fun a:int, b:int -> a*b end), 2);;",new IntValue(2));
			testCase("fun f:FunT(int)-> f() end(fun -> 2 end);;",new IntValue(2));
	//Add a comment to this line
	}


	@Test
    public void testFunction() throws Exception {
        testCase("decl f = fun -> 1 end in f() + 1 end;;", new IntValue(2));
        testCase("decl f = fun x : int -> x + 1 end in f(1) end;;", new IntValue(2));
        testCase("(fun f:FunT(int,int,int) -> f (2,3) end) (fun x:int, y:int -> x+y end);;", new IntValue(5));
        testCase("decl f = (fun f:FunT(int,int,int) -> f (2,3) end) in f(fun x:int, y:int -> x+y end)end;;", new IntValue(5));
        testCase("decl f = fun x : int -> x + 1 end in decl g = fun f : FunT(int,int) -> f(1) end in g (f) end end;;", new IntValue(2));
        testCase("decl f = fun x : int, y : int -> x + y end in f (1,2) + f (1,3) end;;", new IntValue(7));
        testCase("decl f = fun x : int -> fun y : int -> x + y end end in decl g = f (1) in g (2) + g(3) end end;;", new IntValue(7));
        testCase("fun f:FunT(int,int) -> f(2) end (fun x:int-> x + 1 end);;",new IntValue(3));
        testCase("fun x:int,y:int,z:FunT(int,int,int) -> z(x,y) end (1,2,(fun x:int, y:int -> x+y end));;",new IntValue(3));
    }

	@Test
    public void testFunctionEquality() throws Exception {
        testCase("decl x = 1 in x + 1 end == (fun x:int -> x+1 end)(1);;", new BoolValue(true));
        testCase("decl x = 1 in x + 1 end == decl x = (fun x:int -> x+1 end) in x(1) end;;", new BoolValue(true));
        testCase("(fun x:int -> x+1 end)(1) == decl x = (fun x:int -> x+1 end) in x(1) end;;", new BoolValue(true));
    }



}
