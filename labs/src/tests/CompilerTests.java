package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

import main.Compiler;
import parser.ParseException;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.UndeclaredIdentifierException;
import values.IntValue;

public class CompilerTests {

	// This test function was designed to work with Unix like systems. 
	
	private void testCase(String expression, String result) 
			throws IOException, InterruptedException, ParseException, FileNotFoundException, TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		
		Process p;
		
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "rm *.j *.class"});
	    p.waitFor();	    
	    System.out.println(expression);
	    System.out.println("Compiling to Jasmin source code");

	    Compiler.compile(expression);
		
	    System.out.println("Compiling to Jasmin bytecode");
	    
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "java -jar jasmin.jar *.j"});
	    p.waitFor();	    
	    assertTrue("Compiled to Jasmin bytecode", p.exitValue() == 0);

	    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

	    StringBuffer output = new StringBuffer();
        String line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + ";;");
        }
	    System.out.println(output.toString());

		p = Runtime.getRuntime().exec(new String[] {"sh","-c", "java Demo"});
	    p.waitFor();

	    reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

	    output = new StringBuffer();
        line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + ";;");
        }
	    System.out.println("Output: #"+output.toString()+"#");
	    
	    assertTrue(result.equals(output.toString()));
	}
	
	private void testCase(String expression, int value) throws FileNotFoundException, IOException, InterruptedException, ParseException, TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase(expression, value+";;");		
	}

	@Test
	public void BasicTest() throws IOException, InterruptedException, ParseException, TypingException, UndeclaredIdentifierException, DuplicateIdentifierException{
		testCase("1;;", "1;;");
	}

	@Test
	public void testsLabClass02() throws Exception {
		testCase("1+2;;",3);
		testCase("1+2+3;;",6);
		testCase("(1+2)+4;;",7);
		testCase("1+(2+4)+5;;",12);
	}

	@Test
	public void testsLabClass03() throws Exception {
		testCase("1-2-3;;",-4);
		testCase("4*2;;",8);
		testCase("4/2/2;;",1);
		testCase("(20+20)/(4*5);;", 2);
	}
	
	@Test
	public void testsLabClass05() throws Exception {
 		testCase("2 > 1;;",1);
 		testCase("3 > 1;;",1);
 		testCase("3 > 5;;",0);
 		testCase("3 == 3;;",1);
 		testCase("3 == 4;;",0);
 		testCase("3 < 1;;",0);
 		testCase("3 < 5;;",1);
 		testCase("3 != 3;;",0);
 		testCase("3 != 5;;",1);

	}
	
	@Test
	public void testsLabClass06() throws Exception {
 		testCase("2 >= 2;;",1);
 		testCase("2 <= 2;;",1);
	}

	@Test
	public void testsLabClass04() throws Exception {
		testCase("true;;",1);
		testCase("true && true;;", 1);
		testCase("true && false;;", 0);
		testCase("false && true;;", 0);
		testCase("false && false;;", 0);
		testCase("true && (true && false);;",0);
	}
	
	@Test
    public void testFunction() throws Exception {
		
		//teste do exemplo do stor
		
		testCase("decl x = 1 in decl f = fun y:int -> y +x end in decl g = fun h:FunT(int,int) -> h(2) end in g(f) end end end;;",3);
		
		//outro exemplo 
		testCase("decl x = 1 in decl f = fun y:int -> y +x end in decl g = fun x:int-> x+1 end in g(f) end end end;;",3);

        testCase("decl f = fun -> 1 end in f() + 1 end;;", 2);
        testCase("decl f = fun x:int -> x + 1 end in f(2) end;;",3);
        testCase("decl f = fun x : int -> x + 1 end in f(1) end;;",2);
        testCase("(fun f:FunT(int,int,int) -> f (2,3) end) (fun x:int, y:int -> x+y end);;",5);
        testCase("decl f = (fun f:FunT(int,int,int) -> f (2,3) end) in f(fun x:int, y:int -> x+y end)end;;",5);
        testCase("decl f = fun x : int -> x + 1 end in decl g = fun f : FunT(int,int) -> f(1) end in g (f) end end;;",2);
        testCase("decl f = fun x : int, y : int -> x + y end in f (1,2) + f (1,3) end;;",7);
        testCase("decl f = fun x : int -> fun y : int -> x + y end end in decl g = f (1) in g (2) + g(3) end end;;",7);
        testCase("fun f:FunT(int,int) -> f(2) end (fun x:int-> x + 1 end);;",3);
        testCase("fun x:int,y:int,z:FunT(int,int,int) -> z(x,y) end (1,2,(fun x:int, y:int -> x+y end));;",3);
    }
	
	@Test
	public void testsLabClass07() throws Exception {
		testCase("decl x = true in x && false end;;",0);
		testCase("decl x = false in decl y = true in x || y end end;;",1);
		testCase("decl x = decl y = 3 in y != 2 end in x && true end;;",1);
		testCase("decl x = 1 in x < 2 end && decl y = 1 in 2 <= y end;;",0);
		testCase("decl x = decl x = 1 in x >= 2 end y = decl x = 34 in x*17 end in decl z = 3 in !x && (y == z) end end;;",0);
		testCase("decl x = !true y = false in !(x || y) end;;",1);
		testCase("decl x = decl z = 2 in !(2 <= z) end y = !true in !(x || y) end;;",1);
		testCase("decl x = !true y = decl z = 2 in !(2 <= z) end in decl w = 2 t = !y in (w != 1) && !(x || y) || t end end;;",1);
		testCase("decl x =var(0) a=var(0) in *a end;;",0);
		testCase("decl x =var(0) a=var(var(1)) in **a end;;",1);
	
	
		testCase("*var(0);;",0);
		testCase("*var(99);;",99);
		testCase("var(0):=1;;",1);
		testCase("decl x = var(0) in x := 1; *x end;;",1);
		testCase("decl x = var(0) in decl y = x in x := 1; *y end end;;",1);
		testCase("decl x = var(0) in decl y = var(0) in x := 1; *y end end;;",0);
		testCase("decl x = true in if (x) then 5 else 6 end end;;",5);	
		testCase("decl x = 6 in decl x = decl z = 2 in z * z * z end y = decl z = 7 + x in z * x end in y + x end end;;",86);	
		//testCaseBool("decl x = var(0) in decl y = var(0) in while *x < 10 do y := *y + 2; x := *x + 1 end end end;;",false);
		testCase("if true then 1 else 2 end;;",1);
		testCase("if false then 1 else 2 end;;",2);
		//testNegativeCase("if true then true else 1 end;;", new IntValue(0)); // type test
		//testCaseBool("decl x = var(3) in decl y = var(1) in while *x > 0 do y := *y * *x; x := *x - 1 end end end;;",false);
	
	}
	
	@Test
	public void testsLabClass08() throws Exception {

		testCase("*var(0);;",0);
		testCase("*var(99);;",99);
		testCase("var(0):=1;;",1);
//		testCase("decl x=var(0) y=var(true) z=var(x)w=var(var(1))in w:=x z:=*w end;;");
		testCase("decl x = var(0) in x := 1; *x end;;",1);
		testCase("decl x = var(0) in decl y = x in x := 1; *y end end;;",1);
		testCase("decl x = var(0) in decl y = var(0) in x := 1; *y end end;;",0);
		testCase("decl x = true in if (x) then 5 else 6 end end;;",5);
		testCase("decl x = var(0) in decl y = var(0) in while *x < 10 do y := *y + 2; x := *x + 1 end end end;;",0);
		testCase("if true then 1 else 2 end;;", 1);
		testCase("if false then 1 else 2 end;;", 2);
		testCase("decl x = var(3) in decl y = var(1) in while *x > 0 do y := *y * *x; x := *x - 1 end end end;;",0);
		testCase("fun x:int -> x end(1);;",1);
//Add a comment to this line
}

	
	
	
	
}





