package main;

import java.io.ByteArrayInputStream;

import ast.ASTNode;
import parser.ParseException;
import parser.Parser;
import parser.TokenMgrError;
import util.DuplicateIdentifierException;
import util.Environment;
import util.UndeclaredIdentifierException;
import types.IType;
import types.TypingException;
import util.Environment;
import util.IEnvironment;
import values.IValue;
import values.IntValue;

public class Console {

	@SuppressWarnings("static-access")
	public static void main(String args[]) {
		Parser parser = new Parser(System.in);
		while (true) {
			try {
				ASTNode n = parser.Start();
				n.typecheck(new Environment<>());
				System.out.println("OK! " + n.eval(new Environment<>()));
			} catch (TokenMgrError e) {
				System.out.println("Lexical Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (ParseException e) {
				System.out.println("Syntax Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (TypingException e) {
				// TODO Auto-generated catch block
				System.out.println("Typing Error!");
				e.printStackTrace();
			} catch (UndeclaredIdentifierException e) {
				System.out.println("Undeclared Identifier "+e.getMessage());
				e.printStackTrace();
			} catch (DuplicateIdentifierException e) {
				System.out.println("Duplicate Identifier "+e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public static boolean accept(String s) throws ParseException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			parser.Start();
			return true;
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		}
	}

	public static boolean acceptCompare(String s, IValue value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			return n.eval(new Environment<>()).equals(value);
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean acceptCompareBoolInter(String s, IValue value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			return n.eval(new Environment<>()).equals(value);
		} catch (TokenMgrError e) {
			System.out.println(e);
			return false;
		} catch (ParseException e) {
			System.out.println(e);
			return false;
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}

	public static boolean acceptCompareTypes(String s, IType type) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));

			try {
				ASTNode n = parser.Start();
				System.out.println("string: "+s+" type: "+n.typecheck(new Environment<>()));
				return n.typecheck(new Environment<>()).equals(type);
			} catch (TokenMgrError e) {
				e.printStackTrace();
				System.out.println(e);
				return false;
			} catch (ParseException e) {
				System.out.println(e);
				e.printStackTrace();
				return false;
			} catch (Exception e) {
				System.out.println(e);
				return false;
			}

	}

	public static boolean acceptCompareBool(String s, IValue value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			return n.typecheck(new Environment<>()).equals(value);
		} catch (TokenMgrError e) {
			System.out.println(e);
			return false;
		} catch (ParseException e) {
			System.out.println(e);
			return false;
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}


}
