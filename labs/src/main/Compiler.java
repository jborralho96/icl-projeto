package main;

import compiler.*;
import parser.ParseException;
import parser.Parser;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.UndeclaredIdentifierException;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import ast.ASTNode;

public class Compiler {
	/*
	 * (Needed for compatibility with WindowsCompilerTests.java)
	 *
	 * Parametrise it with the directory in which your jasmin.jar resides,
	 * relative to your project's top directory
	 *
	 * e.g. My eclipse project is a directory called ICL with
	 * subdirectories notes, slides and labs. My labs folder
	 * has a subdirectory src, and jasmin.jar is inside src.
	 */
//	private static final String dir = "/home/carrie/icl-projeto/labs/src/";
	private static final String dir = "/home/jambe/Documents/7ºSemestre/ICL/project/icl-projeto/labs/src/";

  public static void main(String args[]) {
    Parser parser = new Parser(System.in);
    ASTNode exp;

    try {
      exp = parser.Start();

      CodeBlock code = new CodeBlock();
      exp.typecheck(new Environment<IType>());
      exp.compile(code,new Environment<>());
      code.dump(dir+"DemoLab4.j");

    } catch (Exception e) {
    	e.printStackTrace();
      System.out.println ("Syntax Error!");
    }
  }

	public static void compile(String s) throws ParseException, FileNotFoundException, TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		ASTNode n = parser.Start();

        CodeBlock code = new CodeBlock();
        n.typecheck(new Environment<IType>());
		n.compile(code,new Environment<>());
		code.dump(dir+"DemoLab4.j");
	}

}
