package ast;

import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTSeq implements ASTNode {

	ASTNode e1, e2;
	IType type;
	public ASTSeq(ASTNode e1, ASTNode e2) {
		// TODO Auto-generated constructor stub
		this.e1=e1;
		this.e2=e2;

	}
	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		// TODO Auto-generated method stub
		e1.eval(env);
		return e2.eval(env);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException{
		IType t1 =e1.typecheck(env);
		IType t2 = e2.typecheck(env);

		if (t2!=null)
			return (type =t2);
		else throw new TypingException("Wrong types in seq");
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<IValue> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

		//para compilar seq(E1,E2)
		//[E1] -> left.compile()
		e1.compile(code, env);
		//pop -> code.emit.pop
		code.emit_pop();
		//[E2] -> right.compile()
		e2.compile(code, env);

	}

	@Override
	public IType getType() {
		return type;
	}

}
