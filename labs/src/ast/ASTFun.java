package ast;

import java.util.LinkedList;
import java.util.List;

import compiler.CodeBlock;
import compiler.CodeBlock.ClosureClass;
import compiler.CodeBlock.FunInterface;
import types.FunType;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.Closure;
import values.IValue;

public class ASTFun implements ASTNode {
	//Fun
	ASTNode body;
	public  List<Param> params;
	IType type;

	public ASTFun(List<Param> params,ASTNode body) {
		this.body = body;
		this.params = params;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		return new Closure(body, params, env);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub
		IEnvironment<IType> envlocal = env.beginScope();
		for (Param p: params){
			envlocal.assoc(p.getId(),p.getType());
		}
		IType t1 = body.typecheck(envlocal);
		if (t1==null)
			throw new TypingException("Wrong type in fun");
		else {
			LinkedList<IType> tipos = new LinkedList<IType>();
			//colocar o vetor de tipos e o resultado
			//é o t1
			//return new FunType(t1);
			for (Param p: params){
				tipos.add(p.getType());
			}
			tipos.add(t1);
			return(type = new FunType(tipos));
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<IValue> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

		//f = fun x -> F1 g = fun y:int -> F2 in decl h = fun x:int -> fun y :ref(int) -> E3

		//criar interfaces dependendo do tipo da funcao

		//FuntT(int,int) -> typefun_0
		//FuntT(int,bool) -> typefun_1
		//FunT(int,FunT(Ref(int)) ->typefun_2
		//FunT(ref(int),int) -> typefun_3


		//classe closure_0
		//.method apply(I) I
		//new frame_1
		//dup
		//init
		//dup
		//aload 1;argument
		//putfield frame_1/loc00 I
		//;SF
		//astore 1 ;next to args
		//[E1]
		//return

		//classe closure_1


		//classe closure_2
		//.implements typefun_2
		//.method apply(I) Ltypefun_3;

		//preparacao do stack frame do closure

		//;argumentos do SF
		//new frame_4
		//dup
		//init
		//dup
		//  ;inicializar o SL do SL do closure
		//dup
		//aload 0 ;this
		//getfield closure_2/SL Lframe_0;
		//putfield frame_4/SL Lframe_0;
		//dup
		//aload 1 ;get the x
		//putfield frame_4/loc_00 I
		//astore 2

		//compilacao do corpo da funcao

		//new closure_3
		//dup
		//init
		//dup
		//aload 2 ;get the SP
		//putfield closure_2/SL Lframe_4;
		//return



		//.method main
		//[decl .... in decl .....]


		//ASTDecl

		//new frame_0
		//dup
		//init
		//dup
		//new closure_0 (ASTFun)
		//dup
		//init ...
		//putfield frame_0/loc Ltypefun_0;
		//dup
		//new closure_1
		//dup
		//init
		//putfield frame_0/loc_01 Ltypefun_1;
		//astore 1 ;SP after arguments and "this"
		//

		//ASTDecl

		//new frame_3
		//dup
		//init
		//dup
		//aload 1 ;get SP
		//checkcast frame_0
		//putfield frame_3/SL Lframe_0; ;store SL

		//ASTFun com SL

		//new closure_2
		//dup
		//init
		//dup
		//aload 1 ; get SP
		//putfield closure_2/SL Lframe_0 ; a funcao h pode usar as funcoes f e g

		//putfield frame_3/loc_0 Ltypefun2;
		//astore 1 ;push SP
		//[E4]

		//;endscope frame_3
		//;endscore frame_0


		FunType type = (FunType)this.getType();  //from the typechecking fase
		List<IType> types = type.paramTypesT();
		IType retType = type.getResultT();

		FunInterface	closureSignature = code.createOrReuse(type);
		//searches "funs" and gets the interface, or creates the
		//.interface funtype_X file with the signature


		//ClosureClass closureCode = code.createClosure(closureSignature)
		ClosureClass closureCode = code.createClosure(closureSignature);
		//creates the class, computes the SP
		//to the defining context

		code.emitnewClosure(closureCode); //new closure_0 --dup--init--dup--aload SP -- putfield SL
		//to the closure context

		IEnvironment<IValue>newenv = env.beginScope();
		//for each parameter
		int loc = 0;
		List<IType> tipoParametros = new LinkedList<IType>();
		for (Param p:params){
			newenv.assoc(p.getId(),null);
			tipoParametros.add(p.type);
		}

		//a frame é que vai ter os locs
		//createFrame and associate SL, push Frame
		//tambem deve receber os tipos dos parametros (por agora)
		//emit inside closure
		//initialize arguments and SL inside closure	
		
		code.pushClosure(closureCode, tipoParametros);
		//colocar a frame na lista
		code.putFrameInList();
		
		//store SP
		code.storeSP();
		
		//compile function body
		body.compile(code, newenv);

		//pop frame (chamar o frame end)

		code.popClosure();

		//back to main (or previous closure)

		//fun x: int -> x (1)
		//fun y -> (fun x -> x ) (y) (1)
		//(fun x -> fun h -> h(x)))(1)(fun x -> x +1)

		//fun y:FunT(int,int) => (fun x:int => x) end (y)(1);;
	}

	@Override
	public IType getType() {
		return type;
	}

}
