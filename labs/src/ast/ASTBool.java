package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.IEnvironment;
import values.BoolValue;
import values.IValue;

public class ASTBool implements ASTNode {

	boolean val;

	public ASTBool(boolean n) {
		val = n;
	}

	@Override
	public String toString() {
		return Boolean.toString(val);
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) {
		return new BoolValue(val);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException {
		return BoolType.singleton;
	}

	@Override
	public void compile(CodeBlock code,IEnvironment<IValue> env) {
		// TODO Auto-generated method stub
		code.emit_bool(val);
	}

	@Override
	public IType getType() {
		return BoolType.singleton;
	}

}
