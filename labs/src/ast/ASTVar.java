package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.MemoryImpl;

public class ASTVar implements ASTNode {

	ASTNode e1;
	IType type;
	
	public ASTVar(ASTNode e1) {
		// TODO Auto-generated constructor stub
		this.e1 = e1;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IValue l = e1.eval(env);
		return MemoryImpl.singleton.newRef(l);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException{
			IType t = e1.typecheck(env);	
			type = new RefType(t);
			return type;
	}
	
	@Override
	public void compile(CodeBlock code, IEnvironment<IValue> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub
		
		//para cada tipo de var, gerar uma refclass 
		
		//var(var(var(1))) 
		// int -> ref_0
		//ref(int) -> ref_1
		//ref(ref(int)) -> ref_2
			
		//gerar uma classe ref_0
		//.class ref_0
		//.public field value int
		//init
		
		//fazer método que cria uma classe Ref e depois guarda no hashmap
		//temos que saber o tipo d
		code.createRef(type);
		
		//.class ref_1
		//.public field value Lref0;
		//
		
		//.class ref_2
		//.public field value Lref1;
		//
		
		//tipos de tj
		// inteiro "I"
		//booleano "I" ou "Z"
		//outros L"class"
		
		// var(E) ---> type(E) = t
			
		
		//new "refclass"
		//dup
		//invokspecial init
		
		code.initReference(type);
		//[E]
		e1.compile(code, env);
		//putfield refclass/value tj       # tipos de tj 
		//
		code.putField(type);

	}

	@Override
	public IType getType() {
		return type;
	}

}
