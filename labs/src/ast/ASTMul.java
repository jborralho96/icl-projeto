package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTMul implements ASTNode {

	ASTNode left, right;
	IType type;

	public ASTMul(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " * " + right.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IValue l = left.eval(env);
		IValue r = right.eval(env);
		if( l instanceof IntValue && r instanceof IntValue)
			return new IntValue(((IntValue)l).getValue() * ((IntValue)r).getValue());
		else {
			return null;
		}
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);

		if(l==IntType.singleton && r==IntType.singleton) {
			type = IntType.singleton;
			return type;
		}
		else
			throw new TypingException("Wrong types in multiplication");
	}

	@Override
	public void compile(CodeBlock code,IEnvironment<IValue> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code,env);
		right.compile(code,env);
		code.emit_mul();
	}

	@Override
	public IType getType() {
		return type;
	}

}
