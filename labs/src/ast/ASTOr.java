package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTOr implements ASTNode {

	ASTNode left, right;
	IType type;

	public ASTOr(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " || " + right.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IValue l= left.eval(env);
		IValue r= right.eval(env);
		if(l instanceof BoolValue && r instanceof BoolValue) {
			return new BoolValue(((BoolValue)l).getValue() || ((BoolValue)r).getValue());
		}else {
			return null;
		}
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);

		if(l==BoolType.singleton && r==BoolType.singleton) {
			type= BoolType.singleton;
			return type;
		}
		else throw new TypingException("Wrong types in or");
	}

	@Override
	public void compile(CodeBlock code,IEnvironment<IValue> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub
		String labelExit,labelTrue;

		labelExit= code.labelFactory.getLabel();
		labelTrue= code.labelFactory.getLabel();

		left.compile(code,env);
		code.emit_ifneq(labelTrue);

		right.compile(code,env);
		code.emit_ifneq(labelTrue);

		code.emit_bool(false);

		code.emit_jump(labelExit);
		code.emit_anchor(labelTrue);

		code.emit_bool(true);

		code.emit_anchor(labelExit);
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}

}
