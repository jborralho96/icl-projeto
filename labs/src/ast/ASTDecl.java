package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.BoolType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTDecl implements ASTNode {

	static class Declaration

	{
		public Declaration(String id, ASTNode def) {
			this.id = id;
			this.def = def;
		}
		String id;
		ASTNode def;
		IType type;
		public IType getType() {
			return type;
		}

	}

	ArrayList<Declaration> decls;
	ASTNode body;
	IType type;

	

	public ASTDecl() {
		decls = new ArrayList<>();
	}

	//adiciona
	public void addBody(ASTNode body) {
		this.body = body;
	}

	//adicionar declaracoes
	public void newBinding(String id, ASTNode e) {
		decls.add(new Declaration(id,e));
	}

	//alterar para por IValues
	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		IEnvironment<IValue> newenv = env.beginScope();

		for( Declaration d: decls)
			newenv.assoc(d.id, d.def.eval(env));

		IValue value = body.eval(newenv);

		env.endScope();

		return value;
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IEnvironment<IType> newenv = env.beginScope();
		for( Declaration d: decls) {			
			newenv.assoc(d.id, d.type = d.def.typecheck(env));
		}

		type = body.typecheck(newenv);

		env.endScope();

		return type;
	}

	@Override
	public void compile(CodeBlock code,IEnvironment<IValue> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException { // <<< IEnvironment<Integer> env

		ArrayList<IType> nodes = new ArrayList<IType>();

		//calcular logo as string dos types
		
		ArrayList<String> teste = new ArrayList<String>();
		StackFrame f =code.newFrame(teste,null); // initialize SL in the stackframe
		/*
		dup
		aload SP
		putfield frame_id/SL Lframe_up;
		 */
		code.initializeSL(f);

		// beginScope
		// For each declaration:
		int counter = 0;
		ArrayList<String> ids= new ArrayList<String>();//identificadores

		for(Declaration decl: decls) {
			//fazer um metodo que associa a frame com o id
			//sipush da definicao
			//putfield frame correspondente/loc_0n  T (em que t = o tipo da definicao)
			code.emit_dup();
			ASTNode def =	decl.def;
			// compilar a expressao (decl x = Expression)
			//newenv.assoc(decl.id, null);
			def.compile(code,env);
			code.declaration(counter, decl.type);
			ids.add(decl.id);
			nodes.add(decl.type);
			// assoc id_i to loc_i (fresh)
			// add loc_i to frame (use the type)

			// compile its expression and generate code that stores the value
			// in the original environment
			/*
				  dup
				  [[ E_i ]]
				  putfield frame_id/loc_i type;
			 */

			// terminate by storing the stack pointer
			/*

			astore 0
			 */
			counter++;
		}
		code.setArgumentsFrame(f, nodes);
		code.storeSP();
		IEnvironment<IValue> newenv = env.beginScope();

		for(String id: ids)
			newenv.assoc(id, null);
		//metodo que insere o frame na lista
		code.putFrameInList();
		body.compile(code,newenv);
		env.endScope();
		// For the main declaration body
		// compiles it, in an environment that "knows" that loc_i corresponds to id_i
		/*
		[[ E ]] 
		 */
		// this corresponds to the endScope
		/*
		aload SP
		checkcast frame_iditem
		getfield frame_id/SL Lframe_up;
		astore SP
		 */
		code.finalize();
	}

	@Override
	public IType getType() {
		return type;
	}
}
