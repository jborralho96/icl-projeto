package ast;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import compiler.CodeBlock;
import types.FunType;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.Closure;
import values.IValue;

public class ASTCall implements ASTNode {

	//chamar o metodo apply da closure
	//template do ASTCall
	//tem left e right
	ASTNode fun;
	List<ASTNode> params;
	IType type;
	public ASTCall(List<ASTNode> l,ASTNode fun) {
		this.params = l;
		this.fun = fun;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		// TODO Auto-generated method stub
		IValue funEval = fun.eval(env);
		if(funEval instanceof Closure) {
			Closure funClosure = ((Closure) funEval);
			IEnvironment<IValue> envLoc = ((Closure) funClosure).getEnv().beginScope();
			List<Param> paramsClosure = funClosure.getParams();
			Iterator<Param> itClosure = paramsClosure.iterator();
			Iterator<ASTNode> itCall = params.iterator();
			while(itClosure.hasNext() && itCall.hasNext())
			{
				envLoc.assoc(itClosure.next().getId(), itCall.next().eval(env));
			}
			IValue value = funClosure.getBody().eval(envLoc);
			((Closure) funClosure).SetEnv(envLoc.endScope());
			return value;
		} else {
			// error
			System.out.println("\n ERROR FUNCTION \n");
			return null;
		}
	}

	@Override
	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType t1 = fun.typecheck(env);

		if(t1 instanceof FunType) {

			if(params.size()==((FunType) t1).paramTypesT().size()){

				for(int i=0;i<params.size();i++) {//verificar se o tipo dos argumentos da chamada sao 
					//iguais aos argumentos da funcao
					IType t = params.get(i).typecheck(env);
					IType r = ((FunType) t1).paramTypesT().get(i);
					if (!t.equals(r))
						throw new TypingException("Wrong types in call");
				}
				return(type = ((FunType) t1).getResultT()); //devolver o tipo que a funcao retorna (ex: fun int->int devolve int)
			}
			else throw new TypingException("Lack of arguments");
		}

		else return null;

		//verificar se t1 é do tipo (int -> bool) por exemplo
		//e se t2 é do tipo int

		//se for verdade devolve bool

	}

	@Override
	public void compile(CodeBlock code, IEnvironment<IValue> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

		//[compilacao do left]
		fun.compile(code, env);		
		//[compilacao do right]
		for(ASTNode p:params){
			p.compile(code, env);
		}

		//devolver o tipo dos argumentos 
		FunType f =(FunType) fun.getType();
		code.emitInvokeInterface("apply",f);
		//invokespecial apply(Tp)Tr


		//return
		//left.compile()
		//for(ASTNode arg: arguments)
		//			arg.compile()
		//emit.invokeSpecial("apply")

		//devolver o tipo 
		//left.getType() == FunT(Tp)Tr
		//TPj = toJasmin(Tp)
		//Trj = toJasmin(Tr)

		//Exemplo de ASTCall()

		//decl x = 1 in  //frame_0
		//					decl f = fun y - > y+x end in  //frame_1, 2
		//					decl g = fun h - > end in      //frame_3, 4
		//g(f)


		//.classDemo
		//.method main
		// new frame_0
		// dup
		// init
		//dup
		//sipush 1
		//putfield frame_0/loc00 I
		//astore 1; push SP (args+1)
		//

		//;second decl
		//f = fun y -> y+x

		//new frame _1
		//		dup
		//		init
		//		dup
		//		new closure_0
		//		dup
		//		init
		//		dup
		//		aload 1
		//		putfield closure_0/SL Lframe0;



		//;second decl endScope
		//aload 1
		//checkcast frame_0
		//astore 1

		//;first decl endScope
		//aconst_null
		//astore 1

	}

	@Override
	public IType getType() {
		return type;
	}

}
