package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTIf implements ASTNode {
	ASTNode e1;
	ASTNode e2;
	ASTNode e3;
	IType type;
	public ASTIf(ASTNode e1, ASTNode e2, ASTNode e3) {
		// TODO Auto-generated constructor stub
		this.e1 = e1;
		this.e2 = e2;
		this.e3 = e3;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IValue l = e1.eval(env);
		IValue l2;
		if(((BoolValue) l).getValue() == true) {
			l2 = e2.eval(env);
		} else {
			l2 = e3.eval(env);
		}

		return l2;
	}

	@Override
	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

		IType t1 = e1.typecheck(env);
		
		if(t1 != BoolType.singleton)
			throw new TypingException("Wrong types in if");
		else {
			IType t2 = e2.typecheck(env);
			IType t3 = e3.typecheck(env);
			
			if(t2==null || t3==null || t2!=t3)
				throw new TypingException("Wrong types in if");
			else return(type = t2);
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<IValue> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub
		String lFalse,lExit;
		//if true then 10 else 20 

		//criar duas labels 

		//lfalse = new Label
		//lexit = new Label
		lFalse= code.labelFactory.getLabel();
		lExit= code.labelFactory.getLabel();
		
		// [E1] sipush 1 //faz push da expressao E1
		e1.compile(code, env);
		//		testa se a expressao é true 
		//		if false jmp false
		code.emit_ifeq(lFalse);
		// [E2] sipush 10 
		e2.compile(code, env);
		//		goto exit:
		code.emit_jump(lExit);
		
		//false:
		code.emit_anchor(lFalse);
		// [E3] sipush 20
		e3.compile(code, env);
		//	exit:
		code.emit_anchor(lExit);
	}

	@Override
	public IType getType() {
		return type;
	}

}
