package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTAnd implements ASTNode {

	ASTNode left, right;
	IType type;
	public ASTAnd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " && " + right.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IValue l= left.eval(env);
		IValue r= right.eval(env);
		if(l instanceof BoolValue && r instanceof BoolValue) {
			return new BoolValue(((BoolValue)l).getValue() && ((BoolValue)r).getValue());
		}else {
			return null;
		}
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);

		if((l==BoolType.singleton) && (r==BoolType.singleton)) {
			type = BoolType.singleton;
			return type;			
			
		}
		else throw new TypingException("Wrong types in and");
	}

	@Override
	public void compile(CodeBlock code,IEnvironment<IValue> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		String labelExit,labelFalse;

		labelExit= code.labelFactory.getLabel();
		labelFalse= code.labelFactory.getLabel();

		left.compile(code,env);
		code.emit_ifeq(labelFalse);

		right.compile(code,env);
		code.emit_ifeq(labelFalse);

		code.emit_bool(true);

		code.emit_jump(labelExit);
		code.emit_anchor(labelFalse);

		code.emit_bool(false);

		code.emit_anchor(labelExit);
	}

	@Override
	public IType getType() {
		return type;
	}
}
