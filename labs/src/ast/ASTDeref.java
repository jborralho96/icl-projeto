package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.MemoryImpl;
import values.RefValue;

public class ASTDeref implements ASTNode {

	ASTNode e1;
	IType type;

	public ASTDeref(ASTNode e1) {
		// TODO Auto-generated constructor stub
		this.e1=e1;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		// TODO Auto-generated method stub
		IValue l = e1.eval(env);
		return MemoryImpl.singleton.get((RefValue) l);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException{
		IType ref = e1.typecheck(env);
		return  type = ((RefType) ref).getType();
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<IValue> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub
		//deref tem uma unica expressao
		//[E]
		e1.compile(code, env);
		//getfield refclass/value tj (tal como o var e o assign, saber o tipo)
		IType tipo = new RefType(type); 
		code.getReference(tipo);
	}

	@Override
	public IType getType() {
		return type;
	}

}
