package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.MemoryImpl;
import values.RefValue;

public class ASTAssign implements ASTNode {

	ASTNode e1, e2;
	IType type;


	public ASTAssign(ASTNode e1, ASTNode e2) {
		// TODO Auto-generated constructor stub
		this.e1=e1;
		this.e2 = e2;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		// TODO Auto-generated method stub
		IValue v1 = e1.eval(env);
		IValue v2 = e2.eval(env);
		return MemoryImpl.singleton.set((RefValue) v1,v2);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

		IType t1 = e1.typecheck(env);
		IType t2 = e2.typecheck(env);
		
		if(t1 instanceof RefType) {
			if(t2.equals(((RefType) t1).getType()))
				return(type = t2);
			else throw new TypingException("Wrong types in assign");
		}
		else return null;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<IValue> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

		//temos o E1 e E2

		//[E1]
		e1.compile(code, env);
		//dup
		code.emit_dup();
		//[E2]
		e2.compile(code, env);
		IType tipo = new RefType(type); 
		//putfield refclass/value tj (verificar o tipo da variavel como no var)
		code.putField(tipo);
		//getfield refclass/value tj
		code.getReference(tipo);

	}

	@Override
	public IType getType() {
		return type;
	}

}
