package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.BoolValue;

public class ASTNot implements ASTNode {

	ASTNode val;
	IType type;

	public ASTNot(ASTNode n) {
		val = n;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		return new BoolValue(!((BoolValue)val.eval(env)).getValue());
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		type = val.typecheck(env);
		return type;
	}

	@Override
	public String toString() {
		return "!" + val.toString();
	}

	@Override
	public void compile(CodeBlock code,IEnvironment<IValue> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub
		String labelExit,labelTrue;

		labelExit= code.labelFactory.getLabel();
		labelTrue= code.labelFactory.getLabel();
		val.compile(code,env);
		code.emit_ifeq(labelTrue);


		code.emit_bool(false);

		code.emit_jump(labelExit);
		code.emit_anchor(labelTrue);

		code.emit_bool(true);

		code.emit_anchor(labelExit);
	}

	@Override
	public IType getType() {
		return type;
	}

}
