package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.MemoryImpl;

public class ASTWhile implements ASTNode {

	ASTNode e1, e2;
	IType type;

	public ASTWhile(ASTNode e1, ASTNode e2) {
		this.e1=e1;
		this.e2=e2;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		// TODO Auto-generated method stub

		while(((BoolValue)e1.eval(env)).getValue() == true) {
			e2.eval(env);
		}
		return new BoolValue(false);

	}

	@Override
	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

		IType t1 = e1.typecheck(env);
		if (t1!=BoolType.singleton)
			throw new TypingException("Wrong types in while");
		else {
			IType t2 = e2.typecheck(env);
			if(t2==null) throw new TypingException("Wrong types in while");
			else{ 
				return (type = BoolType.singleton);			
			}
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<IValue> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub
		String lBeginning,lExit;
		//while(E1,E2)

		//duas labels:
		// beginnings = Label
		//	lexit = label
		lBeginning= code.labelFactory.getLabel();
		lExit= code.labelFactory.getLabel();

		//lbeginning:
		code.emit_anchor(lBeginning);
		//[E1]
		e1.compile(code, env);
		//#if false goto exit 
		code.emit_ifeq(lExit);
		//[E2]
		e2.compile(code, env);
		//pop -> code.emit.pop
		code.emit_pop();
		//goto beginning
		code.emit_jump(lBeginning);
		//exit:
		code.emit_anchor(lExit);
		// return false
		code.emit_bool(false);

	}

	@Override
	public IType getType() {
		return type;
	}

}
