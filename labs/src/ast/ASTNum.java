package ast;

import util.IEnvironment;
import values.IValue;
import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import values.IValue;
import values.IntValue;

public class ASTNum implements ASTNode {

	int val;

	public ASTNum(int n) {
		val = n;
	}

	@Override
	public String toString() {
		return Integer.toString(val);
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) {
		return new IntValue(val);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException {
		return IntType.singleton;
	}

	@Override
	public void compile(CodeBlock code,IEnvironment<IValue> env) {
		code.emit_push(val);
	}

	@Override
	public IType getType() {
		return IntType.singleton;
	}
}
