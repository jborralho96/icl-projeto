.class public Demo
.super java/lang/Object

;
; standard initializer
.method public <init>()V
   aload_0
   invokenonvirtual java/lang/Object/<init>()V
   return
.end method

.method public static main([Ljava/lang/String;)V
       ; set limits used by this method
       .limit locals 10
       .limit stack 256

       ; setup local variables:

       ;    1 - the PrintStream object held in java.lang.out
       getstatic java/lang/System/out Ljava/io/PrintStream;

       ; place your bytecodes here
       ; START

       new frame_1
       dup
       invokespecial frame_1/<init>()V
       dup
       iconst_1
       putfield frame_1/loc_00 
       astore 0
       aload 0
       checkcast frame_1
       getfield frame_1/loc_00 
       ifeq label5
       iconst_0
       ifeq label5
       iconst_1
       goto label4
       label5:
       iconst_0
       label4:
       aconst_null
       astore 0
       ; END


       ; convert to String;
       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
       ; call println 
       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

       return

.end method
